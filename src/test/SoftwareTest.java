package test;

import gui.SoftwareFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import model.CashCard;

public class SoftwareTest {
	
	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new SoftwareTest();
	}

	public SoftwareTest() {
		frame = new SoftwareFrame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(250, 250);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		/*
		 * Student may modify this code and write your result here
		 * frame.setResult("aaa\t bbb\t ccc\t"); frame.extendResult("ddd");
		 */
		CashCard c1 = new CashCard();
		frame.setResult(c1.toString());
		frame.extendResult(c1.deposit(500));
		frame.extendResult(c1.toString());
		frame.extendResult(c1.withdraw(300));
		frame.extendResult(c1.toString());

	}

	ActionListener list;
	SoftwareFrame frame;
}
